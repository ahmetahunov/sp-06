package ru.ahmetahunov.sp.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.sp.enumerated.RoleType;
import javax.persistence.*;

@Entity
@Setter
@Getter
@Cacheable
@NoArgsConstructor
@Table(name = "app_role")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Role extends AbstractEntity {

	@NotNull
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@NotNull
	@Column(name = "type")
	@Enumerated(value = EnumType.STRING)
	private RoleType type = RoleType.USER;

}
