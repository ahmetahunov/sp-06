package ru.ahmetahunov.sp.repository;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.ahmetahunov.sp.config.DatabaseTestConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = DatabaseTestConfig.class)
public class AbstractTestRepository {
}
