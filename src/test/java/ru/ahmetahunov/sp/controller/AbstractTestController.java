package ru.ahmetahunov.sp.controller;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ahmetahunov.sp.config.WebAppConfig;
import ru.ahmetahunov.sp.config.WebSecurityConfig;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
		WebSecurityConfig.class,
		WebAppConfig.class
})
public class AbstractTestController {
}
