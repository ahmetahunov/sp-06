package ru.ahmetahunov.sp.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.ahmetahunov.sp.config.WebAppConfig;
import ru.ahmetahunov.sp.config.WebSecurityConfig;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
		WebSecurityConfig.class,
		WebAppConfig.class
})
public class SecurityTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void prepare() {
		this.mockMvc = MockMvcBuilders
				.webAppContextSetup(wac)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();
	}

	@Test
	public void wrongPasswordTest() throws Exception {
		mockMvc.perform(formLogin("/login").user("user").password("unknown"))
				.andExpect(unauthenticated());
	}

	@Test
	public void correctPasswordTest() throws Exception {
		mockMvc.perform(formLogin("/login").user("admin").password("admin"))
				.andExpect(authenticated());
	}

	@Test
	public void wrongUserTest() throws Exception {
		mockMvc.perform(formLogin("/login").user("unknown").password("user"))
				.andExpect(unauthenticated());
	}

	@Test
	public void withoutUserDataTest() throws Exception {
		mockMvc.perform(get("/projects"))
				.andExpect(status().is4xxClientError());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void userTest() throws Exception {
		mockMvc.perform(get("/projects"))
				.andExpect(status().isOk())
				.andExpect(authenticated().withRoles("USER"));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void userFailedWithoutAdminRoleTest() throws Exception {
		mockMvc.perform(get("/users"))
				.andExpect(status().is4xxClientError());
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void adminRoleTest() throws Exception {
		mockMvc.perform(get("/users"))
				.andExpect(status().isOk())
				.andExpect(authenticated().withRoles("USER", "ADMINISTRATOR"));
	}

}
